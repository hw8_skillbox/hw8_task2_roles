#!/bin/bash
nginx_arr=($(aws ec2 describe-instances --region eu-central-1  --filters Name=tag:Name,Values="Nginx Server in Auto Scalling Group" --query 'Reservations[*].Instances[].PublicIpAddress' | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' -))
reactjs_arr=($(aws ec2 describe-instances --region eu-central-1  --filters Name=tag:Name,Values="ReactJS Server" --query 'Reservations[*].Instances[].PublicIpAddress' | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' -))

echo "[nginx_servers]" > ../ansible/hosts
i=1
for item in ${nginx_arr[*]}
do
    echo "nginx_"$i"  ansible_host="$item >> ../ansible/hosts
    let i=i+1
done

echo "" >> ../ansible/hosts
echo "[reactjs_servers]" >> ../ansible/hosts
i=1
for item in ${reactjs_arr[*]}
do
    echo "reactjs_"$i"  ansible_host="$item >> ../ansible/hosts
    let i=i+1
done

