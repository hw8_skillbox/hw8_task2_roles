provider "aws" {
  region = "eu-central-1"
}

data "aws_availability_zones" "hw8_available_zones" {}

data "aws_ami" "hw8_ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

resource "aws_security_group" "hw8_SG_http_ssh" {
  name = "hw8_reactjs_SG"

  dynamic "ingress" {
    for_each = ["22", "80"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Web and SSH access"
  }
}

resource "aws_launch_configuration" "hw8_LC_web" {
  name_prefix     = "NGINX-"
  image_id        = data.aws_ami.hw8_ubuntu.id
  instance_type   = "t3.micro"
  security_groups = [aws_security_group.hw8_SG_http_ssh.id]
  user_data       = file("user_data.sh")
  key_name        = "ubnt20-home"

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_autoscaling_group" "hw8_ASG_web" {
  name                 = "ASG-${aws_launch_configuration.hw8_LC_web.name}"
  launch_configuration = aws_launch_configuration.hw8_LC_web.name
  min_size             = 2
  max_size             = 2
  min_elb_capacity     = 2
  health_check_type    = "ELB"

  vpc_zone_identifier = [aws_default_subnet.hw8_availabel_zone_1.id, aws_default_subnet.hw8_availabel_zone_2.id]
  load_balancers      = [aws_elb.hw8_LB_web.name]

  provisioner "local-exec" {
    command = "./get_ip.sh"
  }

  dynamic "tag" {
    for_each = {
      Name = "Nginx Server in Auto Scalling Group"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "hw8_LB_web" {
  name               = "HW8-AWS-LB"
  availability_zones = [data.aws_availability_zones.hw8_available_zones.names[0], data.aws_availability_zones.hw8_available_zones.names[1]]
  security_groups    = [aws_security_group.hw8_SG_http_ssh.id]

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    target              = "HTTP:80/"
    interval            = 70
  }

  tags = {
    Name = "Nginx-hw8-ELB"
  }
}

resource "aws_default_subnet" "hw8_availabel_zone_1" {
  availability_zone = data.aws_availability_zones.hw8_available_zones.names[0]
}

resource "aws_default_subnet" "hw8_availabel_zone_2" {
  availability_zone = data.aws_availability_zones.hw8_available_zones.names[1]
}

resource "aws_eip" "hw8_reactjs_EIP" {
  instance = aws_instance.hw8_reactjs_web_instance.id
  tags = {
    Name = "ReactJS Server IP for Ansible"
  }
}

resource "aws_instance" "hw8_reactjs_web_instance" {
  ami                    = data.aws_ami.hw8_ubuntu.id
  instance_type          = "t3.micro"
  key_name               = "ubnt20-home"
  vpc_security_group_ids = [aws_security_group.hw8_SG_http_ssh.id]

  tags = {
    Name = "ReactJS Server"
    Env  = "Production"
    Tier = "Backend"
    CM   = "Ansible"
  }

  lifecycle {
    create_before_destroy = true
  }
}

output "web_loadbalancer_url" {
  value = aws_elb.hw8_LB_web.dns_name
}

output "reactjs_web_site_ip" {
  description = "Elatic IP address assigned to our ReactJS Server"
  value       = aws_eip.hw8_reactjs_EIP.public_ip
}
